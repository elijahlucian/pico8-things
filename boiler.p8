pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
a={}
a.x=0
a.y=0
a.s=3

dude={}
bullet={}

dudes={}
bullets={}

function move_ball()
	if btn(0) then	a.x-=1	end
	if btn(1) then	a.x+=1	end
	if btn(2) then	a.y-=1	end
	if btn(3) then	a.y+=1	end
end

function _update()
	move_ball()
end

function bullet.new()
	s={}
	s.x=0
	s.y=0
	s.v=0
end

function dude.new()
	s={}
	s.x=0
	s.y=0
	self.move = function()
	end
	
	return self
end

function _draw()
	cls()
	circfill(a.x,a.y,a.s,15)
end
