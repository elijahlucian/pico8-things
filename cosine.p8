pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
function _init()
	col = 4
	x = 30
	y = 64
	t = 0
	s = 0
end

function _update()
	p = t/600
	if p%0.1 < 0.015 then
		col = rnd(6)+1
	end
-- x=64*sin(t/1000*6.28+6.28)+64
 x=58*cos(p*6.28)+68
	y=8*sin(p*6.28)+48
	x2=x*0.8
	y2=y+10
	s=cos(p*6.28/2+3.14)*8
	t+=1
end

function _draw()

--	cls()
	print(p%0.1,0,20,5)
	rectfill(x,y,x+s,y+s,col)
--	rectfill(x2,y2,x2+s,y2+s,col+1)
end

