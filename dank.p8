pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
function _init()
	cls()
	x0 = 20
	y0 = 0
	x1 = 120
	y1 = 128

	colour = 6
end

function _update()

	if x0 > 128 then
		x1 = 120
		x0 = 20
		colour += 1
	end

	if x1 < 20 then
		x1 = 120
		x0 = 20
		colour += 1
	end

	if y0 > 128 then
		y0 = 40
		colour += 1
	end

	if y1 < 40 then
		y1 = 128
		colour += 1
	end
	
	if colour > 13 then
		colour = 6
	end
	
	x0 += 0.1 * rnd(10)
	x1 -= 0.2 * rnd(5)
	y0 += 0.3 * rnd(3)
	y1 -= 0.4 * rnd(10)

end

function _draw()
	print("dank", 3, 120)
	line(x0,y0,x1,y1,colour)
--	line(x1,y0,x0,y1,colour-5)
end
