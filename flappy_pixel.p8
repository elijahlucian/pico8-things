pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
w = 128
h = 128
building_limit = 3
buildings = {}
b = {}

p = {
	x = w/2,
	y = h/2,
	v=0
}
p.jump = function(p)
	-- p.y -= 20
	p.v += 2
end
p.update = function(p)
	p.y += 1 - p.v
	p.v -= 0.05
end
p.draw = function(p)
	rectfill(p.x,p.y,p.x+1,p.y+1)
end

function building_bottom()
	if rnd(1) > 0.5 then return h else return 0 end
end

function check_collision(player, building)
	-- if player.x,y
end

b.new = function()
	local s = {
		bottom=true,
		x=rnd(w),
		y=building_bottom(), 
		size=rnd(40),
	}
	s.update = function(s)
		s.x -= 1
		if s.x < 0 then
			s.x = w
		end
	end
	s.draw = function(s)
		rect(s.x,s.y,s.x+s.size/2,s.y-s.size)
	end
	return s
end

function _init()
	for i=0, building_limit do
		add(buildings, b.new())
	end
end

function _update()
	foreach(buildings, function(b) b:update() end)
	if	btnp(4) then
		p:jump()
	end
	p:update()
end

function _draw()
	cls()
	foreach(buildings, function(b) b:draw() end)
	p:draw()
end
