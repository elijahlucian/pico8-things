pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
function _init()
	draw_finished = false
	sleep_time = 0
	sleep_limit = 180

	layers_of_beauty = 0
	maximum_beauty = 2
	
	cls()
	x0 = 5
	y0 = 5
	x1 = 120
	y1 = 128

	colour = rnd(15)+1
end

function mix_the_paint()
		colour = rnd(15)+1
		layers_of_beauty += 1
end

function draw_the_art()

	x0_direction = 1
	x1_direction = 1
	y0_direction = 1
	y1_direction = 1

	if layers_of_beauty > maximum_beauty then
		-- draw_finished = true	
	end

	if x0 > 20 or x0 < 1 then
		-- x1 = 120
		-- x0 = 20
		x0_direction *= -1
		mix_the_paint()
	end

	if x1 > 20 or x1 < 1 then
		-- x1 = 120
		-- x0 = 20
		x1_direction *= -1
		mix_the_paint()
	end

	if y0 > 20 or y0 < 1 then
		-- y0 = 0
		y0_direction *= -1
		mix_the_paint()
	end

	if y1 > 20 or y1 < 1 then
		-- y1 = 128
		y1_direction *= -1
		mix_the_paint()
	end
	
	x0 += 0.1 * rnd(10) * x0_direction
	x1 -= 0.2 * rnd(5)  * x1_direction
	y0 += 0.3 * rnd(3)  * y0_direction
	y1 -= 0.4 * rnd(10) * y1_direction
	
end

function admire_the_art()

	print("hashtag", 3, 110)
	print("  art  ", 3, 120)

	if sleep_time == sleep_limit then
		cls()
		draw_finished = false
		layers_of_beauty = 0
		sleep_time = 0
	else
		sleep_time += 1
	end
	
end

function _update()

	if draw_finished == true then
		admire_the_art()
	else
		draw_the_art()
	end

end

function _draw()
	-- print(x0)
	-- print(x1)
	-- print(y0)
	-- print(y1)
	-- line(x0,y0,x1,y1,colour)
	-- line(x0,y0,x0 + 1,y0 + 1,colour)
	line(x0,y0,x0,y0,colour)
--	line(x1,y0,x0,y1,colour-5)
end
