pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
::start::

for t=0, 60 do
	x = 64*cos(t/60)+64
	cls()
	circfill(x,10,4,5)
	print(t,0,10,2)
	flip()

end

goto start
