pico-8 cartridge // http://www.pico-8.com
version 16
__lua__

function rnd_direction()
	r = rnd(1)
	if r > 0.5 then
		return 1
	else
		return -1
	end
end

function _init()
	cls()
	x_start = rnd(128)
	y_start = rnd(128)
	x_end = rnd(128)
	y_end = rnd(128)
	col = rnd(15)+1

	x_start_direction = rnd_direction()
	y_start_direction = rnd_direction()
	x_end_direction = rnd_direction()
	y_end_direction = rnd_direction()


	frames = 0
end

function dip_the_brush()
	col = rnd(15) + 1
end

function _update()
	if x_start > 128 then
		x_start_direction = -1
		x_start = 128
		dip_the_brush()
	end

	if x_start < 0 then
		x_start_direction = 1
		x_start = 0
		dip_the_brush()
	end

	if y_start > 128 then
		y_start_direction = -1
		y_start = 128
		dip_the_brush()
	end

	if y_start < 0 then
		y_start_direction = 1
		y_start = 0
		dip_the_brush()
	end

	if x_end > 128 then
		x_end_direction *= -1
		x_end = 128
		dip_the_brush()
	end

	if x_end < 0 then
		x_end_direction = 1
		x_end = 0
		dip_the_brush()
	end

	if y_end > 128 then
		y_end_direction = -1
		y_end = 128
		dip_the_brush()
	end

	if y_end < 0 then
		y_end_direction = 1
		y_end = 0
		dip_the_brush()
	end
	
	x_start += 0.29 * rnd(6) * x_start_direction
	y_start += 0.29 * rnd(6) * y_start_direction
	x_end += 0.29 * rnd(6) * x_end_direction
	y_end += 0.29 * rnd(6) * y_end_direction
	frames += 1
end

function _draw()
	line(x_start,y_start,x_end,y_end,col)
	-- print(frames, 10,10, 5)
end
