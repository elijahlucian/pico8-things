pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
function random_direction()
	r =	rnd(1)
	if r > 0.49 then
		return 1
	else
		return -1
	end
end


function _init()
	cls()
	draw_finished = false
	sleep_time = 0
	sleep_limit = 180

	layers_of_beauty = 0
	maximum_beauty = 2
	
	x0_direction = random_direction()
	y0_direction = random_direction()
	
	x0 = 40
	y0 = 40
	x1 = 120
	y1 = 128

	colour = rnd(15)+1
	x_debounce = false
	y_debounce = false
	x_debounce_count = 0
	y_debounce_count = 0
	debounce_max = 30
end

function mix_the_paint()
		colour = rnd(15)+1
		layers_of_beauty += 1
end

function draw_the_art()

	if layers_of_beauty > maximum_beauty then
		-- draw_finished = true	
	end

	if	x_debounce then
		if x_debounce_count > debounce_max then
			x_debounce = false
		else
			x_debounce_count += 1
		end
	end
	if	y_debounce then
		if y_debounce_count > debounce_max then
			y_debounce = false
		else
			y_debounce_count += 1
		end
	end
	
	if x0 > 127 or x0 < 1 then
		if x_debounce == false then
			x0_direction *= -1
			x_debounce_count = 0
			x_debounce = true
			
		end
		mix_the_paint()
	end

	if y0 > 127 or y0 < 1 then
		if y_debounce == false then
			y0_direction *= -1
			y_debounce_count = 0
			y_debounce = true
		end
		mix_the_paint()
	end

	x0 += 0.3 * rnd(5) * x0_direction
	y0 += 0.3 * rnd(5)  * y0_direction
	
end

function admire_the_art()

	print("hashtag", 3, 110)
	print("  art  ", 3, 120)

	if sleep_time == sleep_limit then
		cls()
		draw_finished = false
		layers_of_beauty = 0
		sleep_time = 0
	else
		sleep_time += 1
	end
	
end

function _update()

	if draw_finished == true then
		admire_the_art()
	else
		draw_the_art()
	end

end

function _draw()
--	cls()
--	print(x0,10,10,4)
--	print(y0,10,20,4)
--	print(x_debounce,10,30,5)
--	print(y_debounce,10,40,5)
	line(x0,y0,x0 + 1,y0 + 1,colour)
end
