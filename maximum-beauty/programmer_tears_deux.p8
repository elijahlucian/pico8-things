pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
function _init()
	draw_finished = false
	sleep_time = 0
	sleep_limit = 180

	layers_of_beauty = 0
	maximum_beauty = 2
	
	cls()
	x0 = 20
	y0 = 0
	x1 = 120
	y1 = 128
	
    x0_direction = 1
	x1_direction = 1
	y0_direction = 1
	y1_direction = 1

    debounce = false
    debounce_cycle = 0 
	colour = rnd(15)+1
end

function mix_the_paint()
		colour = rnd(15)+1
		layers_of_beauty += 1
        debounce = true
end

function check_edges()

end

function paint_the_canvas()

end

function draw_the_art()


    x0 += (1 * x0_direction)
    y0 += (0.5 * y0_direction)
	
    if layers_of_beauty > maximum_beauty then
		-- draw_finished = true	
	end

    if x0 > 32 or x0 < 0 then
        x0_direction *= -1
        mix_the_paint()
    end

    if y0 > 32 or y0 < 0 then
        y0_direction *= -1
        mix_the_paint()
    end

end

function admire_the_art()

	print("hashtag", 3, 110)
	print("  art  ", 3, 120)

	if sleep_time == sleep_limit then
		cls()
		draw_finished = false
		layers_of_beauty = 0
		sleep_time = 0
	else
		sleep_time += 1
	end
end

function _update()
	if draw_finished == true then
		admire_the_art()
	else
		draw_the_art()
	end
end

function _draw()
	cls()
    
    print(x0_direction, 1,3,14)
	print(y0_direction, 1,13,14)
    print(debounce_cycle, 1,23, 13)
    print(debounce, 1,33, 13)

	line(x0,y0,x0 + 1,y0 + 1,colour)
end
