pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
function _init()
	draw_finished = false
	sleep_time = 0
	sleep_limit = 180

	layers_of_art = 0
	maximum_beauty = 3
	
	cls()
	x0 = 20
	y0 = 0
	x1 = 120
	y1 = 128

	colour = 6
end

function draw_the_art()
	if x0 > 128 then
		x1 = 120
		x0 = 20
		colour += 1
	end

	if x1 < 20 then
		x1 = 120
		x0 = 20
		colour += 1
	end

	if y0 > 128 then
		y0 = 0
		colour += 1
	end

	if y1 < 0 then
		y1 = 128
		colour += 1
	end
	
	if colour > 13 then
		colour = 6
		cls()
	end
	
	x0 += 0.1 * rnd(10)
	x1 -= 0.2 * rnd(5)
	y0 += 0.3 * rnd(3)
	y1 -= 0.4 * rnd(10)
end

function admire_the_art()
	if sleep_time == sleep_limit then
		draw_finished = false
	else
		sleep_time += 1
	end
	
end

function _update()

	if draw_finished == true then
		admire_the_art()
	else
		draw_the_art()

end

function _draw()
	print("hashtag", 3, 110)
	print("  art  ", 3, 120)
	line(x0,y0,x1,y1,colour)
--	line(x1,y0,x0,y1,colour-5)
end
