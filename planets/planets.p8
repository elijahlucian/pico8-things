pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
fillp(0x5a5a)

outer_satellites = {}
satellites = {}
inner_satellites = {}

satellite = {}

max_satellites = 5
satellite_radius = 40

orbits = 10

characters = {}

icons = {"▥","∧","░","⧗","◆","★","✽","●","♥","웃","⌂","🐱","ˇ","♪","😐","?"}

colors = {
    "0x00",
    "0x01",
    "0x11",
    "0x12",
    "0x22",
    "0x25",
    "0x55",
    "0x59",
    "0x99",
    "0xaa",
} 

text = {
    "i",
    "l",
    "o",
    "v",
    "e",
    "k",
    "a",
    "e",
    "l",
    "a",
}

angle_offset = 0
rotation_speed = 2
radius_speed = 0.5

satellite.new = function (radius,angle,sprite,color,direction,speed_offset)
	self = {
		x = -5,
		y = -5,
		dia = 5,
		col = color,
		radius = radius,
        radius_direction = 1,
		angle = angle,
        sprite = sprite,
        speed = speed_offset,
        size = 4,
        new_size = 4,
	}

	self.update = function(self,t)
        local radius_offset = sin(angle/360)
        local absolute_radius = abs(self.radius)
        local relative_radius = (absolute_radius / 60)
        local inverted_relative_radius = abs((100 - absolute_radius)/30)
		self.angle = self.angle + relative_radius * 3
		self.x = absolute_radius * (cos(self.angle/360)) + 64
		self.y = absolute_radius * (sin(self.angle/360)) + 64
        self.new_size = self.size * ((self.radius / 60) * 4)
        -- self.radius = self.radius + (radius_speed * self.radius_direction) * inverted_relative_radius 
        if self.radius > 60 then 
            self.radius_direction *= -1 
            self.radius = 60
        end
        if self.radius < 2 then 
            self.radius_direction *= -1 
            self.radius = 2
        end
	end
	
	self.draw = function(self)
		print("♥",self.x-3,self.y-2,self.col)
		-- print(self.sprite,self.x-3,self.y-2,self.col)
		-- circfill(self.x,self.y,self.new_size,self.col)
	end

	return self
end

function init_satellites()
	t = 0.01
    satellites = {}
    inner_satellites = {}
    outer_satellites = {}

    gap = 360 / max_satellites
	
    orbit_direction = 1

    for i=1, orbits do 
        speed_offset = i / 100 + 1
        icon_index = flr(rnd(#icons))
        icon = icons[icon_index]
        del(icons,icon_index)
        orbit_radius = i * 5
        -- increment speed offset
        number_of_satellites_in_orbit = max_satellites * orbits
        for k=1, max_satellites do
            add(satellites, satellite.new(orbit_radius,k*gap,text[i],colors[i],orbit_direction,speed_offset))
        end
        orbit_direction *= -1
    end

end

function _init()
	t = 0.01
    init_satellites()
end

function _update()
    t += 0.0001
    angle_offset += rotation_speed
    rad = t % 180
    if rad == 0 then init_satellites() end
	foreach(satellites, function(sat) sat:update(rad) end)

    if btnp(4) then 
        max_satellites += 1
        init_satellites()
    elseif btnp(5) then
        max_satellites -= 1
        init_satellites()
    end
	-- every time the mouse is clicked add a satellite
	-- every time shoot is pushed remove one
end

function _draw() 
	cls()

	foreach(satellites, function(sat) sat:draw() end)

	-- circ(64,64,30,2)
end
