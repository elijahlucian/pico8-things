pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
function _init()

	t = 1

	col = 4
	x = 30
	y = 64
	t_direction = 1

end

function _update()

	p = t/1200
 x=64*sin((p)*6.28)+63
	y=64*cos(x*6.28)+63
	t+=1*t_direction
end

function _draw()

	cls()
	print(x,10,0,4)
	print(t,10,10,4)
	print(p,10,20,5)
	print(cos((p)*6.28),10,30,6)
	rectfill(x,y,x+2,y+2,col)
end

